import unidecode
import requests
from fake_useragent import UserAgent
import bs4
import pymysql
from datetime import datetime
import json

urls = [
	# #flores
		'https://www.giulianaflores.com.br/linda-phalaenopsis-branca/p24437/?src=DEPT',
		'https://www.giulianaflores.com.br/cesta-de-12-rosas-vermelhas/p26198/?src=DEPT',
		'https://www.giulianaflores.com.br/mini-vaso-de-margaridinhas-amarelas/p24541/?src=DEPT',

	# #Perfumes
		'https://www.casasbahia.com.br/perfumaria/Perfumes/Feminino/wave-for-her-eau-de-parfum-hollister-perfume-feminino-50ml-10099336.html?utm_medium=Cpc&utm_source=GP_PLA&IdSku=10099336&idLojista=18392&utm_campaign=bele_smart-shopping&gclid=CjwKCAiA44LzBRB-EiwA-jJipEsGtd8iMWvdH3Q2BPA2HW9kKTDnZZQ0XIPs5r72O9Ik5SNLMhyClhoC4y4QAvD_BwE',
		'https://www.casasbahia.com.br/perfumaria/Perfumes/Feminino/Mirage-World-Romantic-Rose-Eau-de-Parfum-Vivinevo---Perfume-Feminino-100ml-10098924.html?utm_medium=Cpc&utm_source=GP_PLA&IdSku=10098924&idLojista=18392&utm_campaign=bele_smart-shopping&gclid=CjwKCAiA44LzBRB-EiwA-jJipPLC181NvNWRDvpyY_ixRfoMgkyfWuuADZiJIbkTQZAhl315WrfdIxoCfgMQAvD_BwE',
		'https://www.casasbahia.com.br/perfumaria/Perfumes/Feminino/perfume-cool-madam-feminino-eau-de-toilette-100ml-paris-elysees-11424205.html?utm_medium=Cpc&utm_source=GP_PLA&IdSku=11424205&idLojista=32782&utm_campaign=bele_smart-shopping&gclid=CjwKCAiA44LzBRB-EiwA-jJipGDxk31HRNI4YhNPgcViZHwa9Q7loFQoJMn_NOthMCOygwQsx4Z75xoCFPoQAvD_BwE',
		'https://www.pontofrio.com.br/perfumaria/Perfumes/Feminino/perfume-milano-feminino-eau-de-parfum-prada-12114767.html?utm_source=zoom&utm_medium=comparadorpreco&utm_content=12114767&cm_mmc=zoom_XML-_-PERF-_-Comparador-_-12114767',
	# #secadores
		'https://www.zoom.com.br/secador-cabelo/secador-de-cabelo-com-ar-frio-2200-watts-cadence-obsession-sec601?__zaf_=preco-100-ou-mais%7C%7C_o%3A3',
		'https://www.zoom.com.br/secador-cabelo/secador-de-cabelo-gama-italy-iq-perfetto-profissional-potencia-2000-watts?__zaf_=preco-100-ou-mais%7C%7C_o%3A3',
		'https://www.zoom.com.br/secador-cabelo/secador-de-cabelo-ga-ma-italy-aura-nano-titanium-potencia-2300-watts?__zaf_=preco-50-a-150%7C%7C_o%3A3',
		'https://www.zoom.com.br/secador-cabelo/secador-de-cabelo-ga-ma-italy-keration-3d-pro-potencia-2200-watts-emissao-ions?__zaf_=preco-50-a-150%7C%7C_o%3A3',
		'https://www.zoom.com.br/secador-cabelo/secador-de-cabelo-mondial-sc-10?__zaf_=preco-ate-50%7C%7C_o%3A11',
		'https://www.zoom.com.br/secador-cabelo/secador-de-cabelo-revlon-turbo-revlon?__zaf_=preco-ate-50%7C%7C_o%3A11',
		'https://www.amazon.com.br/Calca-Jeans-Feminina-Levis-Skinny/dp/B00WVXUI1A/ref=olp_product_details?ie=UTF8&me=&th=1',
	# #sapata
		'https://www.americanas.com.br/produto/116590727/scarpin-tufi-duek-camurca?cor=Nude&epar=ZOOM&hl=lower&opn=YSMESP&s_term=YYNKZU&sellerId=0&sellerid=0&tamanho=34',
		'https://www.americanas.com.br/produto/39227514?cor=Preto&epar=ZOOM&hl=lower&opn=YSMESP&s_term=YYNKZU&sellerId=12965420000191&sellerid=12965420000191&tamanho=34',
		'https://www.americanas.com.br/produto/23531851/open-boot-feminino-meia-pata-vazado-carrano-preto125655?cor=Preto&epar=ZOOM&hl=lower&opn=YSMESP&s_term=YYNKZU&sellerId=12965420000191&sellerid=12965420000191&tamanho=33',
		'https://www.americanas.com.br/produto/22838865/sapato-via-marte-14-4303?cor=MARINHO&epar=ZOOM&hl=lower&opn=YSMESP&s_term=YYNKZU&sellerId=87227385000102&sellerid=87227385000102&tamanho=38',
		'https://www.americanas.com.br/produto/125905410/mocassim-azaleia-franja-fivela?cor=Marrom%20Claro&epar=ZOOM&hl=lower&opn=YSMESP&s_term=YYNKZU&sellerId=0&sellerid=0&tamanho=34',
		'https://www.americanas.com.br/produto/125840311/dockside-jeans-dijean-cadarco?cor=Azul%20Marinho&epar=ZOOM&hl=lower&opn=YSMESP&s_term=YYNKZU&sellerId=0&sellerid=0&tamanho=35',
	#bota
		'https://www.americanas.com.br/produto/9425633/bota-linha-neve-forrada-em-la-natural-feminina?cor=Preto&epar=ZOOM&hl=lower&opn=YSMESP&s_term=YYNKZU&sellerId=8699663000158&sellerid=8699663000158&tamanho=35',
		'https://www.dafiti.com.br/Bota-Fiero-Linha-Neve-Forrada-Em-La-Natural-Preto-1979670.html?utm_source=zoom&utm_medium=fd&utm_term=FI534SHF29QNE&utm_content=FI534SHF29QNE',
		'https://www.americanas.com.br/produto/46225230?composicao=Couro&cor=Preto&marca=Bigioni&pfm_carac=Coturno&pfm_page=category&pfm_pos=grid&pfm_type=vit_product_grid&tamanho=34',
		'https://www.americanas.com.br/produto/35780194/bota-feminina-over-the-knee-preta-via-marte-18159?cor=Preto&epar=ZOOM&hl=lower&opn=YSMESP&s_term=YYNKZU&sellerId=7888632002479&sellerid=7888632002479&tamanho=33',
		'https://www.dafiti.com.br/Bota-Dakota-Vernis-Salto-Fino-Preta-3658111.html?utm_source=zoom&utm_medium=fd&utm_term=DA511SHF88DKP&utm_content=DA511SHF88DKP',
	#celulares	
		'https://www.zoom.com.br/celular/smartphone-apple-iphone-8-plus-64gb-4g?__zaf_=smartphone%7C%7C_o%3A11',
		'https://www.zoom.com.br/celular/smartphone-xiaomi-redmi-note-8-64gb-android?__zaf_=smartphone%7C%7Cxiaomi%7C%7C_o%3A11',
		'https://www.zoom.com.br/celular/smartphone-samsung-galaxy-a50-sm-a505g-128gb?__zaf_=smartphone%7C%7C_o%3A11',
	# chocolate
		'https://www.extra.com.br/Alimentos/Bomboniere/Chocolate/chocolate-ferrero-rocher-c-12-ferrero-11267082.html?IdSku=11267082&origin=autocomplete&p=ferrero+rocher&ranking=1&typeclick=3&ac_pos=header',
		'https://www.extra.com.br/Alimentos/Bomboniere/Chocolate/chocolate-lindt-lindor-ao-leite-100g-1500896859.html?IdSku=1500896859',
		'https://www.extra.com.br/Alimentos/Bomboniere/Chocolate/caixa-de-bombom-especialidades-300g-nestle-1501685904.html?IdSku=1501685904',
		'https://www.extra.com.br/Alimentos/Bomboniere/Chocolate/tablete-de-chocolate-ao-leite-100g-garoto-15086113.html?IdSku=15086113'
]

## initializing the UserAgent object
user_agent = UserAgent()
produtos = ''

## starting the loop
for url in urls:

	headers = {
	    'User-Agent': user_agent.opera,
	}

	try:
		page = requests.get(url, headers=headers)
		html = page.text

		# soup = bs4.BeautifulSoup(html, 'html.parser')
		soup = bs4.BeautifulSoup(html, 'lxml')

		if url.find('giulianaflores') != -1 or url.find('cestasmichelli') != -1 :
			nomeProduto = soup.find('span', attrs={'id':'ContentSite_lblProductDsName'}).text
			valorProduto = soup.find('span', attrs={'class':'precoPor_prod'})['content']
		elif url.find('pontofrio') != -1 :
			print(soup)
			nomeProduto = soup.find('b', itemprop='name').text
			valorProduto = soup.find('strong', attrs={'id':'ctl00_Conteudo_ctl00_precoPorValue'}).find('i', attrs={'class':'price'}).text	
		elif url.find('zoom.com.br') != -1 :
			nomeProduto = soup.find('h1', attrs={'class':'product-name'}).find('span').text.strip()
			valorProduto = soup.find('a', attrs={'class':'price-label'}).find('strong').text.strip()
		elif url.find('amazon') != -1 :
			nomeProduto = soup.find('span', attrs={'id':'productTitle'}).text.strip()
			valorProduto = soup.find('span', attrs={'id':'priceblock_saleprice'}).text.strip()
		elif url.find('dafiti') != -1 :
			nomeProduto = soup.find('h1', itemprop='name').text
			valorProduto = soup.find('span', itemprop='price')['content']
		elif url.find('extra.com.br') != -1 :
			nomeProduto = soup.find('b', itemprop='name').text
			valorProduto = soup.find('strong', attrs={'id':'ctl00_Conteudo_ctl00_precoPorValue'}).text
		elif url.find('casasbahia') != -1 :
			nomeProduto = soup.find('b', itemprop='name').text
			valorProduto = soup.find('i', attrs={'class':'sale price'}).text.strip()
		elif url.find('americanas') != -1 :
			jsonProduto = json.loads(soup.find('script', type="application/ld+json").text)
			nomeProduto = jsonProduto['@graph'][4]['name']
			valorProduto = jsonProduto['@graph'][4]['offers']['price']
		
		
		print(nomeProduto)
		print(valorProduto)
		valorNovo = valorProduto.replace('R$', '').replace('.', '').replace(',', '').strip()
		if produtos != "":
			produtos += ",('{}','{}','{:.2f}','{}')".format(unidecode.unidecode(nomeProduto), url, float(valorNovo[:-2]+'.'+valorNovo[-2:]), datetime.now())
		else:
			produtos += "('{}','{}','{:.2f}','{}')".format(unidecode.unidecode(nomeProduto), url, float(valorNovo[:-2]+'.'+valorNovo[-2:]), datetime.now())
	except:
		print('Dados não encontrados')
		produtos += ",('{}','{}','{:.2f}','{}')".format('Produto esgotado', url, 0, datetime.now())
		pass
		
# #Connect to database
db = pymysql.connect("localhost","root","root","estatistica")
cursor = db.cursor()
sql = "INSERT INTO produtos(nome, \
   url, preco, criado_em) \
   VALUES {}".format(produtos)
cursor.execute(sql)
db.commit()
db.close()
print('Feito!')